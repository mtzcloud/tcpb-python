# Python TeraChem Protocol Buffer (TCPB) Client

This repository is designed to facilitate the development a Python client for communicating with TeraChem running in Protocol Buffer server mode.

This client uses C-style sockets for communication, and Protocol Buffers for a clean, well-defined way to serialize TeraChem input & output.

For more documentation, check out https://mtzgrouptcpb.readthedocs.io/en/latest/.

For distribution, check out https://anaconda.org/mtzgroup/tcpb.

## How do I use this?

TODO: More details

## Contact

* Stefan Seritan <sseritan@stanford.edu>
